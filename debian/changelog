python-urlobject (2.4.3-7) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository-Browse.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 20 Nov 2022 12:17:12 +0000

python-urlobject (2.4.3-6) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on python3-setuptools.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 28 Oct 2022 20:07:59 +0100

python-urlobject (2.4.3-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 17 Oct 2022 04:05:32 +0100

python-urlobject (2.4.3-4) unstable; urgency=medium

  [ Louis-Philippe Véronneau ]
  * Update the VCS paths. Closes: #946096

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Sandro Tosi <morph@debian.org>  Mon, 20 Jun 2022 00:22:38 -0400

python-urlobject (2.4.3-3) unstable; urgency=medium

  * No change rebuild.

 -- Andrej Shadura <andrewsh@debian.org>  Sun, 27 Oct 2019 22:54:20 +0100

python-urlobject (2.4.3-2) unstable; urgency=medium

  * Fix FTBFS: install README.rst, not README.md.

 -- Andrej Shadura <andrewsh@debian.org>  Sun, 27 Oct 2019 22:45:52 +0100

python-urlobject (2.4.3-1) unstable; urgency=medium

  [ Sandro Tosi ]
  * Non-maintainer upload.
  * Drop python2 support; Closes: #938245.

  [ Andrej Shadura ]
  * New upstream version.
  * Refresh the patch.
  * Maintain in the team.
  * Run wrap-and-sort -bask.
  * d/copyright: Use https protocol in Format field.
  * Use debhelper-compat instead of debian/compat.
  * Use secure URI in Homepage field.
  * Bump debhelper from old 9 to 12.

 -- Andrej Shadura <andrewsh@debian.org>  Sun, 13 Oct 2019 17:34:49 +0200

python-urlobject (2.4.0-1) unstable; urgency=medium

  * New upstream release.

 -- Andrew Shadura <andrewsh@debian.org>  Tue, 05 Jul 2016 16:57:16 +0200

python-urlobject (2.3.4-1) unstable; urgency=low

  * Initial release.

 -- Andrew Shadura <andrewsh@debian.org>  Sat, 11 Jan 2014 15:20:18 +0100
